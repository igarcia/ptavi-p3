#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):

    def __init__(self):
        """
        Constructor. Inicializamos las variables
        """
        self.list = []
        self.diccelement = {
            'root-layout': ["width", "height", "background-color"],
            'region': ["id", "top", "bottom", "left", "right"],
            'img': ["src", "region", "begin", "dur"],
            'audio': ["src", "begin", "dur"],
            'textstream': ["src", "region"]
        }

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name in self.diccelement:
            dicc = {}
            dicc["etiqueta"] = name
            for atrib in self.diccelement[name]:
                dicc[atrib] = attrs.get(atrib, "")

            self.list.append(dicc)

    def get_tags(self):
        return self.list


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
