#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smallsmilhandler import SmallSMILHandler
from urllib.request import urlretrieve
import sys
import json


class KaraokeLocal():
    def __init__(self, file):
        parser = make_parser()
        smallsmilh = SmallSMILHandler()
        parser.setContentHandler(smallsmilh)
        parser.parse(open(file))

        self.list = smallsmilh.get_tags()

    def __str__(self):
        phrase = ""
        for dictionary in self.list:
            label = dictionary['etiqueta']
            phrase += str(label) + '\t'
            for attribs in dictionary:
                if attribs != "etiqueta":
                    val = dictionary[attribs]
                    phrase += str(attribs) + "=" + '"' + str(val) + '"' + "\t"
            phrase = phrase[:-2] + '\n'
        return phrase

    def to_json(self, filesmil, filejson=''):
        if not filejson:
            filejson = file.replace(".smil", ".json")
        with open(filejson, "w") as filej:
            json.dump(self.list, filej, indent=1)

    def do_local(self):
        for dictionary in self.list:
            for attributes in dictionary:
                if attributes != "etiqueta":
                    if dictionary[attributes].startswith("http://"):
                        url = dictionary[attributes]
                        Download = url.split("/")[-1]
                        urlretrieve(url, Download)
                        dictionary[attributes] = Download


if __name__ == "__main__":
    """
    MAIN PROGRAM
    """
    if len(sys.argv) != 2:
        sys.exit("ussage karaoke.py file.smill")
    else:
        try:
            file = sys.argv[1]
        except IndexError:
            sys.exit("Ussage karaoke.py file.smil")

    karaokelocal = KaraokeLocal(file)

    print(karaokelocal.__str__())

    karaokelocal.to_json(file)

    karaokelocal.do_local()

    filejson = "local.json"
    karaokelocal.to_json(filejson)

    print(karaokelocal)
